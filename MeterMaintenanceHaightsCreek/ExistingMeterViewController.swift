//
//  ExistingMeterViewController.swift
//  MeterMaintenance
//
//  Created by Gregory Pierce on 8/25/16.
//  Copyright © 2016 Greg Pierce. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


class ExistingMeterViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    //MARK: Properties
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var searchTextBox: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var choiceControl: UISegmentedControl!
    @IBOutlet weak var meterNumberLabel: UILabel!
    @IBOutlet weak var ertLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var lastReadValue: UILabel!
    @IBOutlet weak var updateSegControl: UISegmentedControl!
    @IBOutlet weak var newValueTextBox: UITextField!
    @IBOutlet weak var meterChangeOutReasonPickerView: UIPickerView!
    @IBOutlet weak var reasonLabel: UILabel!
    
    
    
    var urlBaseString = "https://services.weberbasin.com/haightscreek/getmeters.svc/json/meters"
    //var urlBaseString = "http://gisprog/getmeters.svc/json/meters"
    var bytes: NSMutableData?
    var data: Data?
    var color: UIColor?
    var temporaryMeterNumber = ""
    var meterReasonDS = ["Dead", "Customer Request", "Malfunctioning"]
    
    override func viewDidLoad() {
        self.activityIndicator.center = self.view.center
        
        super.viewDidLoad()
        
        color = meterNumberLabel.textColor
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ExistingMeterViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        //these notification center observers are for the screen movement when the keyboard is active
        //we need to move the screen up for the new value keyboard to ensure it is still visible
        NotificationCenter.default.addObserver(self, selector: #selector(ExistingMeterViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ExistingMeterViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        meterChangeOutReasonPickerView.tag = 1;
        meterChangeOutReasonPickerView.delegate = self;
        meterChangeOutReasonPickerView.dataSource = self;
        meterChangeOutReasonPickerView.selectRow(1, inComponent: 0, animated: true)
    }
    
    //MARK: Pickerview
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            return meterReasonDS[row];
        }
        return "";
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1
        {
            return meterReasonDS.count;
        }
        return 0;
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    
    
    //MARK: Keyboards
    //methods to ensure textfield is still visible when editing
    //only when newvalue textbox is editing, we don't need to move the keyboard
    //for the search text box
    @objc func keyboardWillShow(_ notification: Notification) {
        
        if searchTextBox.isEditing == false {
            if let keyboardSize = ((notification as NSNotification).userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if view.frame.origin.y == 0{
                    self.view.frame.origin.y -= keyboardSize.height
                }
                else {
                    
                }
            }
        }
        
        }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = ((notification as NSNotification).userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
            else {
                
            }
        }
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    //MARK: Navigation
    @IBAction func cancelButtonPressed(_ sender: AnyObject) {
        //hide the keyboard
        self.dismissKeyboard()
        dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: Actions
    @IBAction func searchButtonPressed(_ sender: UIButton) {
        self.activityIndicator.center = self.view.center
        meterNumberLabel.textColor = color
        ertLabel.textColor = color
        self.dismissKeyboard()
        self.view.isUserInteractionEnabled = false
        self.activityIndicator.startAnimating()
        var result = false
        
        //compensate for a portion of the barcode on the ert that returns 10 digits instead of 8
        if (searchTextBox.text?.characters.count > 8)
        {
            let tempString = searchTextBox.text
            let range = tempString?.characters.index((tempString?.startIndex)!, offsetBy: 2)
            searchTextBox.text = tempString?.substring(from: range!)
        }
        
        //background thread
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: { () -> Void in
            
            if(self.searchTextBox.text != "")
            {
                //if search by Meter number
                if (self.choiceControl.selectedSegmentIndex == 0){
                    let requestURL = self.urlBaseString + "/query/meter/" + self.searchTextBox.text! + "/0/0"
                    //check to see if the url is valid and the website responds.
                    result = self.testURL(requestURL)
                    
                    if result == true{
                        self.data = try! Data(contentsOf: URL(string: requestURL)!)
                    }
                    
                }
                    //if search by ERT number
                else if (self.choiceControl.selectedSegmentIndex == 1) {
                    
                    let requestURL = self.urlBaseString + "/query/ert/" + self.searchTextBox.text! + "/0/0"
                    //check to see if the url is valid and the website responds
                    result = self.testURL(requestURL)
                    
                    if result == true {
                        self.data = try! Data(contentsOf: URL(string: requestURL)!)
                    }
                    
                }
                else {
                    let alert = UIAlertController(title: "Alert", message: "Please choose to search by meter or ERT.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                
                if result == true {
                    //execution after the background thread completes -- spinning activity view indicator
                    DispatchQueue.main.async(execute: { () -> Void in
                        let json = try! JSONSerialization.jsonObject(with: self.data!, options:.allowFragments) as! [String:AnyObject]
                        
                        if let meters = json["GetAllMetersMethodResult"] as? [[String: AnyObject]] {
                            if meters.count < 1 {
                                self.meterNumberLabel.text = ""
                                self.ertLabel.text = ""
                                self.addressLabel.text = ""
                                self.cityLabel.text = ""
                                self.lastReadValue.text = ""
                                //show alert
                                let alert = UIAlertController(title: "Alert", message: "No results returned!", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            else {
                                for meter in meters
                                {
                                    self.meterNumberLabel.text = meter["meterNumber"] as? String
                                    self.ertLabel.text = meter["meterRFERTID"] as? String
                                    self.addressLabel.text = meter["meterServiceAddress"] as? String
                                    self.cityLabel.text = meter["meterServiceCity"] as? String
                                    self.lastReadValue.text = meter["meterBegBal"] as? String
                                    
                                    if (self.meterNumberLabel.text == "") {
                                        self.meterNumberLabel.text = "Not found!"
                                        self.meterNumberLabel.textColor = UIColor.red
                                    }
                                    if (self.ertLabel.text == "") {
                                        self.ertLabel.text = "Not found!"
                                        self.ertLabel.textColor = UIColor.red
                                    }
                                }
                            }
                        }
                        self.activityIndicator.stopAnimating()
                        self.view.isUserInteractionEnabled = true
                    })
                }
                else {
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.activityIndicator.stopAnimating()
                        self.view.isUserInteractionEnabled = true
                        let alert = UIAlertController(title: "Alert", message: "A database connection cannot be established.  Contact the administrator.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    })
                }
            }
            else {
                self.activityIndicator.stopAnimating()
                self.view.isUserInteractionEnabled = true
                let alert = UIAlertController(title: "Alert", message: "Please enter the meter or radio number.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        });
        
    }
    
    func updateRadioAsWell (_alert: UIAlertAction)
    {
        let alert = UIAlertController(title: "Update Radio", message: "Enter the new radio number below:", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        
        alert.addTextField { (textField) in
            textField.keyboardType = UIKeyboardType.numberPad
        }
        
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            
            var result = false
            let requestURL = self.urlBaseString + "/query/meter/12345678/0/0"
            result = self.testURL(requestURL)
            self.meterNumberLabel.text = self.newValueTextBox.text
            
            
            if (result)
            {
                //through the update statement on the web server we change the ert number and then run a query to ensure
                //that it was changed.  the json data from that query is what is returned here.
                
                //added code to determine if it's a sensus or itron radio here
                var finalUpdateValue = textField?.text
                
                //let updateURL = self.urlBaseString + "/update/ert/" + self.meterNumberLabel.text! + "/" + self.newValueTextBox.text!
                let updateURL = self.urlBaseString + "/update/ert/" + self.temporaryMeterNumber + "/" + finalUpdateValue! + "/0"
                self.data = try? Data(contentsOf: URL(string: updateURL)!)
                
                self.ertLabel.text = textField?.text
                
                
                let json = try! JSONSerialization.jsonObject(with: self.data!, options:.allowFragments) as! [String:AnyObject]
                
                
                if let meters = json["GetAllMetersMethodResult"] as? [[String: AnyObject]] {
                    if meters.count < 1 {
                        self.meterNumberLabel.text = ""
                        self.ertLabel.text = ""
                        self.addressLabel.text = ""
                        self.cityLabel.text = ""
                        self.lastReadValue.text = ""
                        //show alert
                        let alert = UIAlertController(title: "Alert", message: "Unable to update the database.  Please contact the administrator.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else
                    {
                        for meter in meters {
                            //lets check to make sure the ert number was updated.
                            
                            let returnedMeterNumber = meter["meterNumber"] as? String
                            let accountMeterNumber = self.temporaryMeterNumber
                            let returnedERTNumber = meter["meterRFERTID"] as? String
                            let inputERTNumber = self.ertLabel.text
                            
                            if (returnedMeterNumber == accountMeterNumber) {
                                if (returnedERTNumber == inputERTNumber) {
                                    let imageView = UIImageView(frame: CGRect(x: 180, y: 17, width: 20, height: 20))
                                    imageView.image = UIImage(named: "GreenCheckImage")
                                    let alert = UIAlertController(title: "Success", message: "Your updates were saved!", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                    alert.view.addSubview(imageView)
                                    self.present(alert, animated: true, completion: nil)
                                    self.ertLabel.text = returnedERTNumber
                                    self.meterNumberLabel.text = returnedMeterNumber
                                    self.ertLabel.textColor = UIColor.green
                                    self.newValueTextBox.text = ""
                                    self.updateSegControl.selectedSegmentIndex = UISegmentedControlNoSegment
                                    self.searchTextBox.text = ""
                                    self.lastReadValue.text = "0"
                                    self.reasonLabel.isHidden = true
                                    self.meterChangeOutReasonPickerView.isHidden = true
                                }
                            }
                            else {
                                let alert = UIAlertController(title: "Alert", message: "The database was not updated.  Check all inputs!", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                    }
                }
            }
            
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    func checkForMeterUpdate (_alert: UIAlertAction)
    {
        let json = try! JSONSerialization.jsonObject(with: self.data!, options:.allowFragments) as! [String:AnyObject]
        
        
        if let meters = json["GetAllMetersMethodResult"] as? [[String: AnyObject]] {
            if meters.count < 1 {
                self.meterNumberLabel.text = ""
                self.ertLabel.text = ""
                self.addressLabel.text = ""
                self.cityLabel.text = ""
                self.lastReadValue.text = ""
                //show alert
                let alert = UIAlertController(title: "Alert", message: "Unable to update the database.  Please contact the administrator.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                for meter in meters {
                    //lets check to make sure the meter number now exists in the account.
                    let returnedMeterNumber = meter["meterRFERTID"] as? String
                    let inputMeterNumber = self.temporaryMeterNumber
                    
                    //check here to make sure that everything returned that which was entered, and thus success.
                    if (returnedMeterNumber == inputMeterNumber)
                    {
                        //alert view showing all data saved.  or animation appears.
                        let imageView = UIImageView(frame: CGRect(x: 180, y: 17, width: 20, height: 20))
                        imageView.image = UIImage(named: "GreenCheckImage")
                        
                        let alert = UIAlertController(title: "Success", message: "Your updates were saved!", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        alert.view.addSubview(imageView)
                        self.present(alert, animated: true, completion: nil)
                        self.meterNumberLabel.text = returnedMeterNumber
                        self.meterNumberLabel.textColor = UIColor.green
                        self.newValueTextBox.text = ""
                        self.updateSegControl.selectedSegmentIndex = UISegmentedControlNoSegment
                        self.searchTextBox.text = ""
                        self.lastReadValue.text = "0"
                        self.reasonLabel.isHidden = true
                        self.meterChangeOutReasonPickerView.isHidden = true
                    }
                    else{
                        let alert = UIAlertController(title: "Alert", message: "The database was not updated.  Check all inputs!", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        
    }
    
    
    //this method called when user hits ok button from alert view that pops up ensuring they want to update.
    func updateDatabase (_ alert: UIAlertAction) {
        self.activityIndicator.center = self.view.center
        self.activityIndicator.startAnimating()
        self.view.isUserInteractionEnabled = false
        var result = false
        
        //background thread
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: { () -> Void in
            
            //if update meter number
            if (self.updateSegControl.selectedSegmentIndex == 0)
            {
                let requestURL = self.urlBaseString + "/query/meter/" + self.searchTextBox.text! + "/0/0"
                result = self.testURL(requestURL)
                
                if(result)
                {
                    var reason = self.meterReasonDS[self.meterChangeOutReasonPickerView.selectedRow(inComponent: 0)]
                    reason = reason.replacingOccurrences(of: " ", with: "%20")
                    let updateURL = self.urlBaseString + "/update/meter/" + self.meterNumberLabel.text! + "/"
                        + self.newValueTextBox.text! + "/" + reason
                    self.data = try? Data(contentsOf: URL(string: updateURL)!)
                }
                
                let alert = UIAlertController(title: "Update Radio?", message: "Do you also want to update the radio for meter number " + self.newValueTextBox.text!, preferredStyle: UIAlertControllerStyle.alert)
                
                //place the current meter number into the temporary meter for use in the updateRadioAsWell function
                self.temporaryMeterNumber = self.newValueTextBox.text!
                
                alert.addAction(UIAlertAction(title: "Update", style: UIAlertActionStyle.default, handler: self.updateRadioAsWell))
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: self.checkForMeterUpdate))
                self.present(alert, animated: true, completion: nil)
                
                
            }
                //if update ert number
            else if (self.updateSegControl.selectedSegmentIndex == 1)
            {
                let requestURL = self.urlBaseString + "/query/meter/" + self.searchTextBox.text! + "/0/0"
                result = self.testURL(requestURL)
                
                if (result)
                {
                    //through the update statement on the web server we change the ert number and then run a query to ensure
                    //that it was changed.  the json data from that query is what is returned here.
                    
                    //added code to determine if it's a sensus or itron radio here
                    var finalUpdateValue = self.newValueTextBox.text
                    
                    //let updateURL = self.urlBaseString + "/update/ert/" + self.meterNumberLabel.text! + "/" + self.newValueTextBox.text!
                    let updateURL = self.urlBaseString + "/update/ert/" + self.meterNumberLabel.text! + "/" + finalUpdateValue! + "/0"
                    self.data = try? Data(contentsOf: URL(string: updateURL)!)
                }
            }
            else
            {
                let alert = UIAlertController(title: "Alert", message: "Please choose to update meter or ERT.  Database not updated!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
            
            if(result) {
                //execution after the background thread completes
                DispatchQueue.main.async(execute: { () -> Void in
                    let json = try! JSONSerialization.jsonObject(with: self.data!, options:.allowFragments) as! [String:AnyObject]
                    
                    
                    if let meters = json["GetAllMetersMethodResult"] as? [[String: AnyObject]] {
                        if meters.count < 1 {
                            self.meterNumberLabel.text = ""
                            self.ertLabel.text = ""
                            self.addressLabel.text = ""
                            self.cityLabel.text = ""
                            self.lastReadValue.text = ""
                            //show alert
                            let alert = UIAlertController(title: "Alert", message: "Unable to update the database.  Please contact the administrator.", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {
                            for meter in meters {
                                if (self.updateSegControl.selectedSegmentIndex == 0) {
                                    
                                    //lets check to make sure the meter number now exists in the account.
                                    let returnedNewMeterNumber = meter["meterRFERTID"] as? String
                                    let inputMeterNumber = self.newValueTextBox.text
                                    let returnedOriginalMeterNumber = meter["meterNumber"] as? String
                                    let originalMeterNumber = self.meterNumberLabel.text
                                    
                                    
                                    //check here to make sure that everything returned that which was entered, and thus success.
                                    if (returnedNewMeterNumber == inputMeterNumber)
                                    {
                                        if (returnedOriginalMeterNumber == originalMeterNumber)
                                        {
                                            //alert view showing all data saved.  or animation appears.
                                            let imageView = UIImageView(frame: CGRect(x: 180, y: 17, width: 20, height: 20))
                                            imageView.image = UIImage(named: "GreenCheckImage")
                                            
                                            let alert = UIAlertController(title: "Success", message: "Your updates were saved!", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                            alert.view.addSubview(imageView)
                                            self.present(alert, animated: true, completion: nil)
                                            self.meterNumberLabel.text = returnedNewMeterNumber
                                            self.meterNumberLabel.textColor = UIColor.green
                                            self.newValueTextBox.text = ""
                                            self.updateSegControl.selectedSegmentIndex = UISegmentedControlNoSegment
                                            self.searchTextBox.text = ""
                                        }
                                    }
                                    else{
                                        let alert = UIAlertController(title: "Alert", message: "The database was not updated.  Check all inputs!", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else if (self.updateSegControl.selectedSegmentIndex == 1) {
                                    //lets check to make sure the ert number was updated.
                                    
                                    let returnedMeterNumber = meter["meterNumber"] as? String
                                    let accountMeterNumber = self.meterNumberLabel.text
                                    let returnedERTNumber = meter["meterRFERTID"] as? String
                                    let inputERTNumber = self.newValueTextBox.text
                                    
                                    if (returnedMeterNumber == accountMeterNumber) {
                                        if (returnedERTNumber == inputERTNumber) {
                                            let imageView = UIImageView(frame: CGRect(x: 180, y: 17, width: 20, height: 20))
                                            imageView.image = UIImage(named: "GreenCheckImage")
                                            let alert = UIAlertController(title: "Success", message: "Your updates were saved!", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                            alert.view.addSubview(imageView)
                                            self.present(alert, animated: true, completion: nil)
                                            self.ertLabel.text = returnedERTNumber
                                            self.ertLabel.textColor = UIColor.green
                                            self.newValueTextBox.text = ""
                                            self.updateSegControl.selectedSegmentIndex = UISegmentedControlNoSegment
                                            self.searchTextBox.text = ""
                                        }
                                    }
                                    else {
                                        let alert = UIAlertController(title: "Alert", message: "The database was not updated.  Check all inputs!", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                }
                            }
                        }
                    }
                    self.activityIndicator.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                })
            }
            else {
                DispatchQueue.main.async(execute: { () -> Void in
                    self.activityIndicator.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                    let alert = UIAlertController(title: "Alert", message: "A database connection cannot be established.  Contact the administrator.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                })
            }
            
            
        });
        
    }
    
    @IBAction func updateTypeSegControlSwitched(_ sender: Any) {
        
        if updateSegControl.selectedSegmentIndex == 0
        {
            meterChangeOutReasonPickerView.isHidden = false
            reasonLabel.isHidden = false;
        }
        else if updateSegControl.selectedSegmentIndex == 1
        {
            meterChangeOutReasonPickerView.isHidden = true
            reasonLabel.isHidden = true
        }
    }
    
    
    
    @IBAction func updateButtonPressed(_ sender: UIButton) {
        
        self.dismissKeyboard()
        
        
        //compensate for a portion of the barcode on the ert that returns 10 digits instead of 8
        
        if (newValueTextBox.text?.characters.count > 8 && updateSegControl.selectedSegmentIndex == 1)
        {
            let tempString = newValueTextBox.text
            let range = tempString?.characters.index((tempString?.startIndex)!, offsetBy: 2)
            newValueTextBox.text = tempString?.substring(from: range!)
        }
        
        
        var choice = ""
        if (updateSegControl.selectedSegmentIndex == 0){
            choice = "meter"
        }
        else if (updateSegControl.selectedSegmentIndex == 1)
        {
            choice = "Radio"
        }
        
        if (choice == "")
        {
            let alert = UIAlertController(title: "Alert", message: "Please choose to update either meter or radio number.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if (self.meterNumberLabel.text != "" && self.newValueTextBox.text != "")
        {
            if updateSegControl.selectedSegmentIndex == 0
            {
                let alert = UIAlertController(title: "Update", message: "You are about to update the " + choice + " number, are you sure?", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Update", style: UIAlertActionStyle.default, handler: updateDatabase))
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }
            else if updateSegControl.selectedSegmentIndex == 1
            {
                let alert = UIAlertController(title: "Update", message: "You are about to update the " + choice + " number, are you sure?", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Update", style: UIAlertActionStyle.default, handler: updateDatabase))
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        else if (self.meterNumberLabel.text == "")
        {
            let alert = UIAlertController(title: "Alert", message: "Please load a meter or radio to replace.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if (self.newValueTextBox.text == "")
        {
            let alert = UIAlertController(title: "Alert", message: "Please check your new meter / radio input.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    
    @IBAction func clearSearchBoxTextButtonPressed(_ sender: UIButton) {
        searchTextBox.text = ""
        self.meterNumberLabel.text = ""
        self.ertLabel.text = ""
        self.addressLabel.text = ""
        self.cityLabel.text = ""
        self.lastReadValue.text = ""
    }
    
    
    //this method to test whether the webserver is responding to the request.
    //if it is not, an error message is triggered in the code above
    func testURL(_ requestURL: String) -> Bool{
        let url: URL = URL(string: requestURL)!
        let request: URLRequest = URLRequest(url: url)
        var response: URLResponse?
        
        do {
            var data = try NSURLConnection.sendSynchronousRequest(request, returning: &response) as Data?
            
            if let httpResponse = response as? HTTPURLResponse
            {
                if httpResponse.statusCode == 200
                {
                    return true
                }
                else
                {
                    return false
                }
            }
        }
        catch {}
        
        return false
    }
    
    //this method if an area is touched outside the keyboard area.
    //    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    //        //hide the keyboard
    //        searchTextBox.resignFirstResponder()
    //    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
