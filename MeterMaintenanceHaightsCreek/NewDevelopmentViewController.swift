//
//  NewDevelopmentViewController.swift
//  MeterMaintenance
//
//  Created by Gregory Pierce on 9/1/16.
//  Copyright © 2016 Greg Pierce. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class NewDevelopmentViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    //MARK: Properties
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var subdivisionTextBox: UITextField!
    @IBOutlet weak var lotOrAddressTextBox: UITextField!
    @IBOutlet weak var meterNumberTextBox: UITextField!
    @IBOutlet weak var ertTextBox: UITextField!
    @IBOutlet weak var radioTypeSegControl: UISegmentedControl!
    @IBOutlet weak var multiplierTextBox: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lastLotLabel: UILabel!
    @IBOutlet weak var lastMeterNumberLabel: UILabel!
    @IBOutlet weak var lastInsertedMeterLabel: UILabel!
    @IBOutlet weak var meterSizeTextBox: UITextField!
    @IBOutlet weak var meterSizePicker: UIPickerView!
    
    
    
    var urlBaseString = "https://services.weberbasin.com/haightscreek/newdevelopmentmeter.svc/json/meters"
    //var urlBaseString = "http://gisprog/newdevelopmentmeter.svc/json/meters"
    var data: Data?
    
    var meterSizeDS = ["3/4", "1", "2", "3", "4", "6", "8", "10", "12"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(NewDevelopmentViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ExistingMeterViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ExistingMeterViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //add functions to see the following textbox values changing
        meterNumberTextBox.addTarget(self, action: #selector(NewDevelopmentViewController.meterTextBoxDidChange), for: UIControlEvents.editingChanged)
        
        ertTextBox.addTarget(self, action: #selector(NewDevelopmentViewController.ertTextBoxDidChange), for: UIControlEvents.editingChanged)
        
        
        //set delegates
        self.subdivisionTextBox.delegate = self
        self.lotOrAddressTextBox.delegate = self
        self.meterNumberTextBox.delegate = self
        self.ertTextBox.delegate = self
        self.multiplierTextBox.delegate = self
        
        self.meterSizeTextBox.delegate = self
        self.meterSizePicker.tag = 1
        self.meterSizePicker.delegate = self
        self.meterSizePicker.dataSource = self
        self.meterSizePicker.selectRow(1, inComponent: 0, animated: true)
    }
    
    //MARK: Keyboards
    //methods to ensure textfield is still visible when editing
    //only when newvalue textbox is editing, we don't need to move the keyboard
    //for the search text box
    func keyboardWillShow(_ notification: Notification) {
        
        if ertTextBox.isEditing == true || multiplierTextBox.isEditing == true {
            if let keyboardSize = ((notification as NSNotification).userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if view.frame.origin.y == 0{
                    self.view.frame.origin.y -= keyboardSize.height
                }
                else {
                    
                }
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = ((notification as NSNotification).userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
            else {
                
            }
        }
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    //MARK: Picker View Delegate Methods
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return meterSizeDS[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return meterSizeDS.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.meterSizeTextBox.text = meterSizeDS[row]
        meterSizePicker.isHidden = true
    }
    
    
    //MARK: Action
    @IBAction func doneButtonPressed(_ sender: UIBarButtonItem) {
        dismissKeyboard()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        self.dismissKeyboard()
        
        var error = ""
        if(subdivisionTextBox.text == "") {
            error = "A subdivision name was not found!"
        }
        else if (lotOrAddressTextBox.text == "") {
            error = "A lot or address was not found!"
        }
        else if (meterNumberTextBox.text == "") {
            error = "A meter number was not found!"
        }
        else if (ertTextBox.text == "") {
            error = "An ERT / AMR number was not found!"
        }
        
        //compensate for a portion of the barcode on the ert that returns 10 digits instead of 8
        if (ertTextBox.text?.characters.count > 8)
        {
            let tempString = ertTextBox.text
            let range = tempString?.characters.index((tempString?.startIndex)!, offsetBy: 2)
            ertTextBox.text = tempString?.substring(from: range!)
        }
        
        if (error != "")
        {
            let alert = UIAlertController(title: "Alert!", message: error, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            do {
            let alert = UIAlertController(title: "Save", message: "Save this new meter?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: addMeter))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            }
            catch is NSError {
                
            }
        }
        
    }
    
    
    //MARK: Functions
    
    func addMeter (_ UIAlert: UIAlertAction){
        self.view.isUserInteractionEnabled = false
        self.activityIndicator.center = self.view.center
        self.activityIndicator.startAnimating()
        
        //a flag to determine if our database update was successful!
        var result = false
        
        //this is the background thread
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: { () -> Void in
            
            //test to see if the webserver is responding with bogus query data.
            let requestURL = self.urlBaseString + "/query/0/0/0/0/0/0"
            result = self.testURL(requestURL)
            
            //if the webserver is responding
            if (result) {
                
                //if the multiplier box is empty we will place a 1 in the field.
                var multiplier = ""
                if (self.multiplierTextBox.text == "") {
                    multiplier = "1"
                }
                else {
                    multiplier = self.multiplierTextBox.text!
                }
                
                //remove spaces from subdivision name
                var subdivisionName = ""
                subdivisionName = (self.subdivisionTextBox.text?.replacingOccurrences(of: " ", with: "_"))!
                subdivisionName = subdivisionName.replacingOccurrences(of: "'", with: "")
                
                //remove spaces from lot or address
                var lotOrAddress = ""
                lotOrAddress = (self.lotOrAddressTextBox.text?.replacingOccurrences(of: " ", with: "_"))!
                
                //compose the update url
                var updateURL = self.urlBaseString + "/update/" + subdivisionName + "/" + lotOrAddress + "/"
                
                
                updateURL.append(self.meterNumberTextBox.text! + "/")
                updateURL.append(self.ertTextBox.text! + "/")
                updateURL.append(multiplier)
                
                var meterSize = self.meterSizeTextBox.text
                if (meterSize?.contains("/"))!
                {
                    meterSize = meterSize?.replacingOccurrences(of: "/", with: "%20")
                }
                updateURL.append("/" + meterSize!)
                
                
                //execute the update statement
                self.data =  try? Data(contentsOf: URL(string: updateURL)!)
                
                
            }
            
            if (result)
            {
                //execution after the background thread completes
                DispatchQueue.main.async(execute: { () -> Void in
                    let json = try! JSONSerialization.jsonObject(with: self.data!, options:.allowFragments) as! [String:AnyObject]
                    
                    if let returnedObjects = json["NewDevMeterMethodResult"] as? [[String: AnyObject]]
                    {
                        if (returnedObjects.count < 1)
                        {
                            //show alert because the meter inserted was not found
                            let alert = UIAlertController(title: "Alert", message: "The database was not updated.  Please contact the administrator.  ErrorCode: WhatThe?!", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        //else we found the meter because the query on the webserver looks for the newly inserted meter value.
                        else
                        {
                            for object in returnedObjects {
                                let returnedLot = object["lot"] as? String
                                let returnedMeter = object["meter"] as? String
                                let returnedErt = object["ert"] as? String
                                let inputMeterNumber = self.meterNumberTextBox.text
                                let inputERTNumber = self.ertTextBox.text
                                
                                
                                //everything matched up and we can report success that a new meter was added to the new development table in the database.
                                if(returnedMeter == inputMeterNumber) {
                                    if (returnedErt == inputERTNumber) {
                                        //alert view showing all data saved.  or animation appears.
                                        let imageView = UIImageView(frame: CGRect(x: 180, y: 17, width: 20, height: 20))
                                        
                                        imageView.image = UIImage(named: "GreenCheckImage")
                                        
                                        let alert = UIAlertController(title: "Success", message: "Your new meter installation was saved.", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                        alert.view.addSubview(imageView)
                                        self.present(alert, animated: true, completion: nil)
                                        
                                        //set the last labels to the appropriate value.
                                        self.lastLotLabel.text = "Lot: " + returnedLot!
                                        self.lastMeterNumberLabel.text = "Meter: " + returnedMeter!
                                        
                                        //empty the input field values.
                                        self.lotOrAddressTextBox.text = ""
                                        self.meterNumberTextBox.text = ""
                                        self.ertTextBox.text = ""
                                        self.multiplierTextBox.text = ""
                                    }
                                    else
                                    {
                                        let alert = UIAlertController(title: "Alert", message: "The database was not updated.  Error Code: Returned value does not match!", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else {
                                    let alert = UIAlertController(title: "Alert", message: "The database was not updated.  Error Code: Returned value does not match!", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                        }
                    }
                    self.activityIndicator.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                })
            }
            else
            {
                DispatchQueue.main.async(execute: { () -> Void in
                    self.activityIndicator.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                    let alert = UIAlertController(title: "Alert", message: "A database connection cannot be established.  Contact the administrator.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                })
            }
        });
        
    }
    
    //this method to test whether the webserver is responding to the request.
    //if it is not, an error message is triggered in the code above
    func testURL(_ requestURL: String) -> Bool{
        let url: URL = URL(string: requestURL)!
        let request: URLRequest = URLRequest(url: url)
        var response: URLResponse?
        
        do {
            var data = try NSURLConnection.sendSynchronousRequest(request, returning: &response) as Data?
            
            if let httpResponse = response as? HTTPURLResponse
            {
                if httpResponse.statusCode == 200
                {
                    return true
                }
                else
                {
                    return false
                }
            }
        }
        catch {}
        
        return false
    }
    
    @objc func ertTextBoxDidChange() {
        if (ertTextBox.text?.characters.count == 8) {
            dismissKeyboard()
        }
    }
    
    @objc func meterTextBoxDidChange() {
        
        
        if (meterNumberTextBox.text?.characters.count == 9) {
            
            dismissKeyboard()
        }
    }

    
    //MARK: Overrides

    
    //even though this is not led with an override, it is in fact one.
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField == subdivisionTextBox) {
            lotOrAddressTextBox.becomeFirstResponder()
        }
        else if (textField == lotOrAddressTextBox) {
            meterNumberTextBox.becomeFirstResponder()
        }
        else if (textField == meterNumberTextBox) {
            ertTextBox.becomeFirstResponder()
        }
        else
        {
            resignFirstResponder()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == meterSizeTextBox
        {
            meterSizePicker.isHidden = false
            textField.endEditing(true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
