//
//  MaintenanceInstalledMetersTableViewController.swift
//  MeterMaintenance
//
//  Created by Gregory Pierce on 7/27/17.
//  Copyright © 2017 Greg Pierce. All rights reserved.
//

import UIKit

class MaintenanceInstalledMetersTableViewController: UITableViewController {
    
    var returnedAccounts = [ProjectSearchedAccounts]()
    var data: Data?
    var urlBaseString = "https://services.weberbasin.com/iosmeterservices/getaccounts.svc/json/accounts/"
    //var urlBaseString = "http://gisprog/getaccounts.svc/json/accounts/"
    
    var addressToPass = ""
    var parcelNumToPass = ""
    var nameToPass = ""
    var cityToPass = ""
    

    @IBOutlet var tableView2: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView2.delegate = self

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        loadData()
    }
    
    //MARK: Functions
    
    func loadData() {
        
        returnedAccounts.removeAll()
        var testString = urlBaseString
        testString.append("query/10/0/0/0/0")
        
        if (self.testURL(testString))
        {
            var queryString = urlBaseString
            queryString.append("getmaintenance/")
            queryString.append("/0/0/0/0/0")
            
            self.data =  try? Data(contentsOf: URL(string: queryString)!)
            
            let json = try! JSONSerialization.jsonObject(with: self.data!, options:.allowFragments) as! [String:AnyObject]
            
            
            if let returnedObjects = json["GetAccountsMethodResult"] as? [[String:AnyObject]]
            {
                for account in returnedObjects {
                    let thisAccount = ProjectSearchedAccounts()
                    thisAccount.address = account["address"] as! String
                    thisAccount.address = thisAccount.address.replacingOccurrences(of: "_", with: " ")
                    thisAccount.city = account["city"] as! String
                    //name in this case is actually the MeterStaffEntered and GPSCollected Field
                    //in the following format  [MeterStaffEntered:GPSCollected] as returned by the web server.
                    thisAccount.name = account["name"] as! String
                    thisAccount.parcel = account["parcel"] as! String
                    self.returnedAccounts.append(thisAccount)
                }
            }
        }
        
        self.tableView2.reloadData()
    }
    
    func testURL(_ requestURL: String) -> Bool{
        let url: URL = URL(string: requestURL)!
        let request: URLRequest = URLRequest(url: url)
        var response: URLResponse?
        
        do {
            var data = try NSURLConnection.sendSynchronousRequest(request, returning: &response) as Data?
            
            if let httpResponse = response as? HTTPURLResponse
            {
                if httpResponse.statusCode == 200
                {
                    return true
                }
                else
                {
                    return false
                }
            }
        }
        catch {}
        
        return false
    }
    
    @objc func gpsButtonPressed(sender: UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = tableView(tableView2, cellForRowAt: indexPath) as! MaintenanceInstalledMetersTableViewCell
        
        var splitValues = [String]()
        splitValues = (cell.topHidden.text?.components(separatedBy: ":"))!
        
        let gps = splitValues[1]
        var displayValue = ""
        var valueSent = ""
        if gps == "True" {
            displayValue = "False"
            valueSent = "0"
        }
        else if gps == "False"
        {
            displayValue = "True"
            valueSent = "1"
        }
        
        let alert = UIAlertController.init(title:"Update", message: "Do you want to update the gps collected value to " + displayValue + "?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler:{ action in
            
            let requestURL = self.urlBaseString + "/query/10/10/10/10/0"
            
            let result = self.testURL(requestURL)
            
            if (result)
            {
                //compose the update url
                var updateURL = self.urlBaseString
                updateURL.append("updatemaintenanceGPS/")
                updateURL.append(cell.bottomLabel.text!)
                updateURL.append("/" + valueSent + "/0/0/0")
                
                //execute the insert statement on the database
                self.data =  try? Data(contentsOf: URL(string: updateURL)!)
            }
            
            if (result)
            {
                //execution after the background thread completes
                //DispatchQueue.main.async(execute: { () -> Void in
                    let json = try! JSONSerialization.jsonObject(with: self.data!, options:.allowFragments) as! [String:AnyObject]
                    
                    if let returnedObjects = json["GetAccountsMethodResult"] as? [[String: AnyObject]]
                    {
                        if (returnedObjects.count < 1)
                        {
                            //show alert because the maintenance installed meter was not updated
                            let alert = UIAlertController(title: "Alert", message: "The database was not updated.  Please contact the administrator.  ErrorCode: Argh!!", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                            //else we found the account because the query on the webserver looks for the newly inserted meter value.
                        else
                        {
                            for object in returnedObjects
                            {
                                let parcelNumber = object["parcel"] as? String
                                let gpsCollected = object["name"] as? String
                                //if everything matches up to the input values we can report success and dismiss the view controller
                                if (parcelNumber == cell.bottomLabel.text! && gpsCollected == displayValue)
                                {
                                    
                                    //alert view showing all data saved.  or animation appears.
                                    let imageView = UIImageView(frame: CGRect(x: 180, y: 17, width: 20, height: 20))
                                    
                                    imageView.image = UIImage(named: "GreenCheckImage")
                                    
                                    let alert = UIAlertController(title: "Success", message: "This meter has now had it's GPS value updated.", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                    
                                    alert.view.addSubview(imageView)
                                    self.present(alert, animated: true, completion: nil)
                                    
                                    self.loadData()
                                }
                                else {
                                    let alert = UIAlertController(title: "Alert", message: "The database was not updated.  Error Code: Returned value does not match!", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                        }
                    }
                //})
            }
            else {
                DispatchQueue.main.async(execute: { () -> Void in
                    self.view.isUserInteractionEnabled = true
                    let alert = UIAlertController(title: "Alert", message: "A database connection cannot be established.  Contact the administrator.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                })
            }
            
            
            
            
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return returnedAccounts.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView2.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MaintenanceInstalledMetersTableViewCell
        
        let account = returnedAccounts[(indexPath as NSIndexPath).row]
        cell.gpsButton.tag = indexPath.row
        
        
        cell.gpsButton.addTarget(self, action: #selector(gpsButtonPressed(sender:)), for: .touchUpInside)
        
        let gpsAndMeterValues = returnedAccounts[(indexPath as NSIndexPath).row].name
        
        var splitValues = [String]()
        splitValues = gpsAndMeterValues.components(separatedBy: ":")
        
        if splitValues[0] == "True" {
            cell.selectionStyle = .none
        }
        else {
            cell.selectionStyle = .gray
        }
        
        cell.configure(account.address, bottomText: account.parcel, meterAndGPS: account.name, city: account.city)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath)! as! MaintenanceInstalledMetersTableViewCell
        
        addressToPass = currentCell.topLabel.text!
        parcelNumToPass = currentCell.bottomLabel.text!
        //name is MeterStaffEntered field
        nameToPass = currentCell.topHidden.text!
        cityToPass = currentCell.bottomHidden.text!
        
        if currentCell.selectionStyle != .none {
        
            performSegue(withIdentifier: "segue", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue"
        {
            // Setup new view controller
            
            let viewController = segue.destination as! AddProjectMeterViewController
            viewController.passedInAddress = addressToPass
            viewController.passedInParcelNum = parcelNumToPass
            //name is actually MeterStaffEntered field
            viewController.passedInName = nameToPass
            viewController.passedInCity = cityToPass
        }
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
