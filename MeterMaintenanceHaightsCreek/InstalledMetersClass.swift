//
//  InstalledMetersClass.swift
//  MeterMaintenance
//
//  Created by Gregory Pierce on 9/19/16.
//  Copyright © 2016 Greg Pierce. All rights reserved.
//

import Foundation

class InstalledMeters {
    var subdivision = ""
    var address = ""
    var meterNumber = ""
    var ertNumber = ""
    var multiplier = ""
}


