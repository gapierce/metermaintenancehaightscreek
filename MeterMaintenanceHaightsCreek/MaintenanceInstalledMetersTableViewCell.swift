//
//  MaintenanceInstalledMetersTableViewCell.swift
//  MeterMaintenance
//
//  Created by Gregory Pierce on 8/8/17.
//  Copyright © 2017 Greg Pierce. All rights reserved.
//

import UIKit

class MaintenanceInstalledMetersTableViewCell: UITableViewCell {

    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var topHidden: UILabel!
    @IBOutlet weak var bottomHidden: UILabel!
    @IBOutlet weak var gpsButton: UIButton!
    @IBOutlet weak var meterStaffEnteredButton: UIButton!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    internal func configure(_ topText: String, bottomText: String, meterAndGPS: String, city: String) {
        topLabel.text = topText
        bottomLabel.text = bottomText
        //nameText is actually MeterStaffEntered field
        topHidden.text = meterAndGPS
        bottomHidden.text = city
        
        
        var splitValues = [String]()
        splitValues = meterAndGPS.components(separatedBy: ":")
        
        if splitValues[0] == "True"
        {
            meterStaffEnteredButton.setImage(UIImage(named: "Meter-Green.png"), for: .normal)
        }
        else {
            meterStaffEnteredButton.setImage(UIImage(named: "Meter-Red.png"), for: .normal)
        }
        
        if splitValues[1] == "True"
        {
            gpsButton.setImage(UIImage(named: "GPS-GREEN.png"), for: .normal)
        }
        else
        {
            gpsButton.setImage(UIImage(named: "GPS-RED.png"), for: .normal)
        }

    }
    
    
    //to cause the tableviewcell seperator to reliably show up.  it was not showing occassionally.
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard let superview = contentView.superview else {
            return
        }
        
        for subview in superview.subviews {
            if String(describing: type(of: subview)).hasSuffix("SeparatorView") {
                subview.isHidden = false
            }
        }
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
