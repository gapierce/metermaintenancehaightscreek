//
//  ExitingMeterTableViewCell.swift
//  MeterMaintenance
//
//  Created by Gregory Pierce on 9/26/16.
//  Copyright © 2016 Greg Pierce. All rights reserved.
//

import UIKit

class InstalledMetersTableViewCell: UITableViewCell {

    //MARK: Properties
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    
    
    
    internal func configure(_ topText: String, bottomText: String) {
        topLabel.text = topText
        bottomLabel.text = bottomText
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
