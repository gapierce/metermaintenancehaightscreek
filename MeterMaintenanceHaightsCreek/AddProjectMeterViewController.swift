//
//  AddProjectMeterViewController.swift
//  MeterMaintenance
//
//  Created by Gregory Pierce on 10/6/16.
//  Copyright © 2016 Greg Pierce. All rights reserved.
//

import UIKit

class AddProjectMeterViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    
    //MARK: Properties
    var passedInAddress = ""
    var passedInParcelNum = ""
    var passedInName = ""
    var passedInCity = ""
    var urlBaseString = "https://services.weberbasin.com/haightscreek/getaccounts.svc/json/accounts"
    //var urlBaseString = "http://gisprog/getaccounts.svc/json/accounts"
    var data: Data?
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var parcelLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var meterTypePicker: UIPickerView!
    @IBOutlet weak var meterSizePicker: UIPickerView!
    
    
    
    @IBOutlet weak var newMeterNumberTextBox: UITextField!
    @IBOutlet weak var newERTNumberTextBox: UITextField!
    @IBOutlet weak var meterTypeTextBox: UITextField!
    @IBOutlet weak var meterSizeTextBox: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    var meterTypeDS = ["EvoQ4", "iPerl"]
    var meterSizeDS = ["3/4", "1", "2", "3", "4", "6", "8", "10", "12"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddProjectMeterViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        //add functions to see the following textbox values changing
        newMeterNumberTextBox.addTarget(self, action: #selector(AddProjectMeterViewController.meterTextBoxDidChange), for: UIControlEvents.editingChanged)
        
        newERTNumberTextBox.addTarget(self, action: #selector(AddProjectMeterViewController.ertTextBoxDidChange), for: UIControlEvents.editingChanged)

        
        
        addressLabel.text = passedInAddress
        parcelLabel.text = passedInParcelNum
        nameLabel.text = passedInName
        cityLabel.text = passedInCity
        
        self.newMeterNumberTextBox.delegate = self
        self.newERTNumberTextBox.delegate = self
        self.meterTypeTextBox.delegate = self
        self.meterSizeTextBox.delegate = self
        
        meterTypePicker.tag = 1;
        meterTypePicker.delegate = self
        meterTypePicker.dataSource = self;
        meterTypePicker.selectRow(2, inComponent: 0, animated: true)
        
        meterSizePicker.tag = 2;
        meterSizePicker.delegate = self
        meterSizePicker.dataSource = self
        meterSizePicker.selectRow(1, inComponent: 0, animated: true)
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
        meterTypePicker.isHidden = true
        meterSizePicker.isHidden = true
    }
    
    @objc func ertTextBoxDidChange() {
        if (newERTNumberTextBox.text?.characters.count == 8) {
            dismissKeyboard()
        }
    }
    
    @objc func meterTextBoxDidChange() {
        if (newMeterNumberTextBox.text?.characters.count == 9) {
            dismissKeyboard()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    //even though this is not led with an override, it is in fact one.
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField == newMeterNumberTextBox) {
            newERTNumberTextBox.becomeFirstResponder()
        }
        else
        {
            resignFirstResponder()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == meterTypeTextBox)
        {
            meterTypePicker.isHidden = false
            textField.endEditing(true)
        }
        else if (textField == meterSizeTextBox)
        {
            meterSizePicker.isHidden = false
            textField.endEditing(true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Pickerview Delegate Methods
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            self.view.endEditing(true)
        if (pickerView.tag == 1)
        {
            return meterTypeDS[row];
        }
        else if (pickerView.tag == 2){
            return meterSizeDS[row]
        }
        else
        {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            if pickerView.tag == 1
            {
            return meterTypeDS.count;
        }
            else if pickerView.tag == 2 {
                return meterSizeDS.count;
        }
        return 0
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            self.meterTypeTextBox.text = self.meterTypeDS[row]
            meterTypePicker.isHidden = true
        }
        else if pickerView.tag == 2
        {
            self.meterSizeTextBox.text = self.meterSizeDS[row]
            meterSizePicker.isHidden = true
        }
    }
    
    //MARK: Actions
    
    @IBAction func saveButtonPressed(_ sender: AnyObject) {
        dismissKeyboard()
        
        var error = ""
        
        if (newMeterNumberTextBox.text?.characters.count != 8)
        {
            error = "Please check the number of digits for the meter value."
        }
        else if (newERTNumberTextBox.text?.characters.count != 8)
        {
            error = "Please check the number of digits for the ERT / AMR value."
        }
        
        if (error != "")
        {
            let alert = UIAlertController(title: "Alert!", message: error, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Save", message: "Save this new meter?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: addMeter))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func addMeter (_ UIAlert: UIAlertAction)
    {
        self.view.isUserInteractionEnabled = false
        self.activityIndicator.center = self.view.center
        self.activityIndicator.startAnimating()
        
        //a flag to determine if our database update was successful!
        var result = false
        
        //this is the background thread
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: { () -> Void in
          
            //test to see if the webserver is responding with bogus query data.
            let requestURL = self.urlBaseString + "/query/10/10/10/0/0"
            
            result = self.testURL(requestURL)
            
            if (result)
            {
                //compose the update url
                var updateURL = self.urlBaseString
                updateURL.append("/insert/")
                updateURL.append(self.parcelLabel.text!)
                updateURL.append("/" + self.newMeterNumberTextBox.text!)
                updateURL.append("/" + self.newERTNumberTextBox.text!)
                updateURL.append("/" + self.meterTypeTextBox.text!)
                
                var meterSize = self.meterSizeTextBox.text
                if (meterSize?.contains("/"))!
                {
                    meterSize = meterSize?.replacingOccurrences(of: "/", with: "%20")
                }
                updateURL.append("/" + meterSize!)
                
                //execute the insert statement on the database
                self.data =  try? Data(contentsOf: URL(string: updateURL)!)
            }
            
            if (result)
            {
                //execution after the background thread completes
                DispatchQueue.main.async(execute: { () -> Void in
                   let json = try! JSONSerialization.jsonObject(with: self.data!, options:.allowFragments) as! [String:AnyObject]
                    
                    if let returnedObjects = json["GetAccountsMethodResult"] as? [[String: AnyObject]]
                    {
                        if (returnedObjects.count < 1)
                        {
                            //show alert because the meter inserted was not found
                            let alert = UIAlertController(title: "Alert", message: "The database was not updated.  Please contact the administrator.  ErrorCode: Argh!!", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        //else we found the meter because the query on the webserver looks for the newly inserted meter value.
                        else
                        {
                            for object in returnedObjects
                            {
                                let meterNumber = object["parcel"] as? String
                                let ertNumber = object["name"] as? String
                                
                                //if everything matches up to the input values we can report success and dismiss the view controller
                                if (meterNumber == self.newMeterNumberTextBox.text)
                                {
                                    if (ertNumber == self.newERTNumberTextBox.text)
                                    {
                                        //alert view showing all data saved.  or animation appears.
                                        let imageView = UIImageView(frame: CGRect(x: 180, y: 17, width: 20, height: 20))
                                        
                                        imageView.image = UIImage(named: "GreenCheckImage")
                                        
                                        let alert = UIAlertController(title: "Success", message: "Your new meter installation was saved.", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: self.dismissMethod))
                                        
                                        alert.view.addSubview(imageView)
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                    else {
                                        let alert = UIAlertController(title: "Alert", message: "The database was not updated.  Error Code: Returned value does not match!", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                }
                                else {
                                    let alert = UIAlertController(title: "Alert", message: "The database was not updated.  Error Code: Returned value does not match!", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                        }
                    }
                    self.activityIndicator.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                })
            }
            else {
                DispatchQueue.main.async(execute: { () -> Void in
                    self.activityIndicator.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                    let alert = UIAlertController(title: "Alert", message: "A database connection cannot be established.  Contact the administrator.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                })
            }
            
            
        });
        
        
    }
    
    func dismissMethod(_ uiAlert: UIAlertAction)
    {
        dismiss(animated: true, completion: nil)
    }
    
    //this method to test whether the webserver is responding to the request.
    //if it is not, an error message is triggered in the code above
    func testURL(_ requestURL: String) -> Bool{
        let url: URL = URL(string: requestURL)!
        let request: URLRequest = URLRequest(url: url)
        var response: URLResponse?
        
        do {
            var data = try NSURLConnection.sendSynchronousRequest(request, returning: &response) as Data?
            
            if let httpResponse = response as? HTTPURLResponse
            {
                if httpResponse.statusCode == 200
                {
                    return true
                }
                else
                {
                    return false
                }
            }
        }
        catch {}
        
        return false
    }
    
    @IBAction func cancelButtonPressed(_ sender: AnyObject) {
        dismissKeyboard()
        dismiss(animated: true, completion: nil)
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
