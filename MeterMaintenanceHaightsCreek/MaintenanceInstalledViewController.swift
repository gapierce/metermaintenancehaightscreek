//
//  MaintenanceInstalledViewController.swift
//  MeterMaintenance
//
//  Created by Gregory Pierce on 7/27/17.
//  Copyright © 2017 Greg Pierce. All rights reserved.
//

import UIKit

class MaintenanceInstalledViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    

}
