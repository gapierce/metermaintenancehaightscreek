//
//  ProjectInstalledListTableViewController.swift
//  MeterMaintenance
//
//  Created by Gregory Pierce on 10/7/16.
//  Copyright © 2016 Greg Pierce. All rights reserved.
//

import UIKit




class ProjectInstalledListTableViewController: UITableViewController {

    var returnedMeters = [ProjectInstalledMeter]()
    var data: Data?
    var urlBaseString = "https://services.weberbasin.com/haightscreek/getaccounts.svc/json/accounts/getall/0/0/0/0/0"
    //var urlBaseString = "http://gisprog/getaccounts.svc/json/accounts/getall/0/0/0/0/0"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (self.testURL(self.urlBaseString)) {
            
            self.data =  try? Data(contentsOf: URL(string: self.urlBaseString)!)
            
            let json = try! JSONSerialization.jsonObject(with: self.data!, options:.allowFragments) as! [String:AnyObject]
            
            
            if let returnedObjects = json["GetAccountsMethodResult"] as? [[String:AnyObject]]
            {
                for meter in returnedObjects {
                    let thisMeter = ProjectInstalledMeter()
                    thisMeter.address = meter["address"] as! String
                    thisMeter.meterNumber = meter["parcel"] as! String //the meter number will be returned as the "parcel" value from the web service.
                    thisMeter.ertNumber = meter["name"] as! String //the ert number will be returned as the "name" value from the web service.
                    self.returnedMeters.append(thisMeter)
                }
            }
            
        }
        
        self.tableView.reloadData()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    //MARK: Functions
    
    func testURL(_ requestURL: String) -> Bool{
        let url: URL = URL(string: requestURL)!
        let request: URLRequest = URLRequest(url: url)
        var response: URLResponse?
        
        do {
            var data = try NSURLConnection.sendSynchronousRequest(request, returning: &response) as Data?
            
            if let httpResponse = response as? HTTPURLResponse
            {
                if httpResponse.statusCode == 200
                {
                    return true
                }
                else
                {
                    return false
                }
            }
        }
        catch {}
        
        return false
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return returnedMeters.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProjectInstalledTableViewCell

        let meter = returnedMeters[(indexPath as NSIndexPath).row]
        
        cell.configure(meter.address, bottomText: "Meter: " + meter.meterNumber + " ERT: " + meter.ertNumber)
        

        return cell
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    //MARK: Actions
    @IBAction func doneButtonPressed(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
