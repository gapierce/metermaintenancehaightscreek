//
//  ViewController.swift
//  MeterMaintenance
//
//  Created by Gregory Pierce on 8/25/16.
//  Copyright © 2016 Greg Pierce. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK: Properties
    @IBOutlet weak var newMeterInstallButton: UIButton!
    @IBOutlet weak var existingMeterChangeoutButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

