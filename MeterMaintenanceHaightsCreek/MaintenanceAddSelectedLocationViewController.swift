//
//  MaintenanceAddSelectedLocationViewController.swift
//  MeterMaintenance
//
//  Created by Gregory Pierce on 7/27/17.
//  Copyright © 2017 Greg Pierce. All rights reserved.
//

import UIKit

class MaintenanceAddSelectedLocationViewController: UIViewController {

    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var parcelNumLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    //MARK: Properties
    var passedInAddress = ""
    var passedInParcelNum = ""
    var passedInCity = ""
    var urlBaseString = "https://services.weberbasin.com/iosmeterservices/getaccounts.svc/json/accounts"
    //var urlBaseString = "http://gisprog/getaccounts.svc/json/accounts"
    var data: Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        addressLabel.text = passedInAddress;
        parcelNumLabel.text = passedInParcelNum;
        cityLabel.text = passedInCity;
        
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    @IBAction func saveButtonPressed(_ sender: Any) {
        dismissKeyboard()
        
        let error = ""
        
        
        
        if (error != "")
        {
            let alert = UIAlertController(title: "Alert!", message: error, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Save", message: "Save this new meter?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: addMeter))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func addMeter (_ UIAlert: UIAlertAction)
    {
        self.view.isUserInteractionEnabled = false
        self.activityIndicator.center = self.view.center
        self.activityIndicator.startAnimating()
        
        //a flag to determine if our database update was successful!
        var result = false
        
        //this is the background thread
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: { () -> Void in
            
            //test to see if the webserver is responding with bogus query data.
            let requestURL = self.urlBaseString + "/query/10/10/10/10/10"
            
            result = self.testURL(requestURL)
            
            if (result)
            {
                //compose the update url
                var updateURL = self.urlBaseString
                updateURL.append("/insertmaintenance/")
                updateURL.append(self.parcelNumLabel.text!)
                let updatedAddress = self.addressLabel.text?.replacingOccurrences(of: " ", with: "_")
                updateURL.append("/" + updatedAddress!)
                let updatedCity = self.cityLabel.text?.replacingOccurrences(of: " ", with: "_")
                updateURL.append("/" + updatedCity! + "/0/0")
                
                //execute the insert statement on the database
                self.data = try? Data(contentsOf: URL(string: updateURL)!)
            }
            
            if (result)
            {
                //execution after the background thread completes
                DispatchQueue.main.async(execute: { () -> Void in
                    let json = try! JSONSerialization.jsonObject(with: self.data!, options:.allowFragments) as! [String:AnyObject]
                    
                    if let returnedObjects = json["GetAccountsMethodResult"] as? [[String: AnyObject]]
                    {
                        if (returnedObjects.count < 1)
                        {
                            //show alert because the meter inserted was not found
                            let alert = UIAlertController(title: "Alert", message: "The database was not updated.  Please contact the administrator.  ErrorCode: Argh!!", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                            //else we found the meter because the query on the webserver looks for the newly inserted meter value.
                        else
                        {
                            for object in returnedObjects
                            {
                                let parcelNumber = object["parcel"] as? String
                                
                                //if everything matches up to the input values we can report success and dismiss the view controller
                                if (parcelNumber == self.parcelNumLabel.text)
                                {
                                    
                                    //alert view showing all data saved.  or animation appears.
                                    let imageView = UIImageView(frame: CGRect(x: 180, y: 17, width: 20, height: 20))
                                    
                                    imageView.image = UIImage(named: "GreenCheckImage")
                                    
                                    let alert = UIAlertController(title: "Success", message: "Your new meter installation was saved.", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: self.dismissMethod))
                                    
                                    alert.view.addSubview(imageView)
                                    self.present(alert, animated: true, completion: nil)
                                    
                                }
                                else {
                                    let alert = UIAlertController(title: "Alert", message: "The database was not updated.  Error Code: Returned value does not match!", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                        }
                    }
                    self.activityIndicator.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                })
            }
            else {
                DispatchQueue.main.async(execute: { () -> Void in
                    self.activityIndicator.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                    let alert = UIAlertController(title: "Alert", message: "A database connection cannot be established.  Contact the administrator.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                })
            }
        });
    }
    
    func dismissMethod(_ uiAlert: UIAlertAction)
    {
        dismiss(animated: true, completion: nil)
    }
    
    //this method to test whether the webserver is responding to the request.
    //if it is not, an error message is triggered in the code above
    func testURL(_ requestURL: String) -> Bool{
        let url: URL = URL(string: requestURL)!
        let request: URLRequest = URLRequest(url: url)
        var response: URLResponse?
        
        do {
            var data = try NSURLConnection.sendSynchronousRequest(request, returning: &response) as Data?
            
            if let httpResponse = response as? HTTPURLResponse
            {
                if httpResponse.statusCode == 200
                {
                    return true
                }
                else
                {
                    return false
                }
            }
        }
        catch {}
        
        return false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
