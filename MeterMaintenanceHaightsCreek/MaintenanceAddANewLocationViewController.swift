//
//  MaintenanceAddANewLocationViewController.swift
//  MeterMaintenance
//
//  Created by Gregory Pierce on 7/27/17.
//  Copyright © 2017 Greg Pierce. All rights reserved.
//

import UIKit

class MaintenanceAddANewLocationViewController: UIViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var returnedAccounts = [ProjectSearchedAccounts]()
    var urlBaseString = "https://services.weberbasin.com/iosmeterservices/getaccounts.svc/json/accounts/"
    //var urlBaseString = "http://gisprog/getaccounts.svc/json/accounts/"
    var data: Data?
    var addressToPass = ""
    var parcelNumToPass = ""
    var nameToPass = ""
    var cityToPass = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchBar.delegate = self
        self.tableView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        returnedAccounts.removeAll()
        self.tableView.reloadData()
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.dismissKeyboard()
        
        var testString = urlBaseString
        testString.append("query/10/0/0/0/0")
        
        if (testURL(testString))
        {
            var queryString = urlBaseString
            queryString.append("query/")
            var searchString = searchBar.text
            if (searchString?.contains(" "))!
            {
                searchString = searchString?.replacingOccurrences(of: " ", with: "_")
            }
            queryString.append(searchString!)
            queryString.append("/0/0/0/0")
            
            self.data =  try? Data(contentsOf: URL(string: queryString)!)
            
            let json = try! JSONSerialization.jsonObject(with: self.data!, options:.allowFragments) as! [String:AnyObject]
            
            
            if let returnedObjects = json["GetAccountsMethodResult"] as? [[String:AnyObject]]
            {
                for account in returnedObjects {
                    let thisAccount = ProjectSearchedAccounts()
                    thisAccount.address = account["address"] as! String
                    thisAccount.city = account["city"] as! String
                    thisAccount.name = account["name"] as! String
                    thisAccount.parcel = account["parcel"] as! String
                    
                    self.returnedAccounts.append(thisAccount)
                }
            }
            
            
            
            self.tableView.reloadData()
            
            if (returnedAccounts.count == 0)
            {
                let alert = UIAlertController(title: "Error", message: "No results found!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        else
        {
            let alert = UIAlertController(title: "Error", message: "Cannot connect to database.  Contact the administrator.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    func testURL(_ requestURL: String) -> Bool{
        let url: URL = URL(string: requestURL)!
        let request: URLRequest = URLRequest(url: url)
        var response: URLResponse?
        
        do {
            var data = try NSURLConnection.sendSynchronousRequest(request, returning: &response) as Data?
            
            if let httpResponse = response as? HTTPURLResponse
            {
                if httpResponse.statusCode == 200
                {
                    return true
                }
                else
                {
                    return false
                }
            }
        }
        catch {}
        
        return false
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        returnedAccounts.removeAll()
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return returnedAccounts.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProjectFindTableViewCell
        
        let account = returnedAccounts[(indexPath as NSIndexPath).row]
        
        cell.configure(account.address, bottomText: account.parcel, nameText: account.name, cityText: account.city)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath)! as! ProjectFindTableViewCell
        
        addressToPass = currentCell.topLabel.text!
        parcelNumToPass = currentCell.bottomLabel.text!
        nameToPass = currentCell.nameHiddenLabel.text!
        cityToPass = currentCell.cityHiddenLabel.text!
        
        performSegue(withIdentifier: "segue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue"
        {
            // Setup new view controller
            
            let viewController = segue.destination as! MaintenanceAddSelectedLocationViewController
            viewController.passedInAddress = addressToPass
            viewController.passedInCity = cityToPass
            viewController.passedInParcelNum = parcelNumToPass
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
