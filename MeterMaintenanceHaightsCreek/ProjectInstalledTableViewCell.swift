//
//  ProjectInstalledTableViewCell.swift
//  MeterMaintenance
//
//  Created by Gregory Pierce on 10/7/16.
//  Copyright © 2016 Greg Pierce. All rights reserved.
//

import UIKit

class ProjectInstalledTableViewCell: UITableViewCell {

    //MARK: Properties
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    
    
    internal func configure(_ topText: String, bottomText: String) {
        topLabel.text = topText
        bottomLabel.text = bottomText
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
