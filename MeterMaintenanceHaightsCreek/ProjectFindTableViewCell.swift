//
//  ProjectFindTableViewCell.swift
//  MeterMaintenance
//
//  Created by Gregory Pierce on 10/5/16.
//  Copyright © 2016 Greg Pierce. All rights reserved.
//

import UIKit

class ProjectFindTableViewCell: UITableViewCell {

    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var nameHiddenLabel: UILabel!
    @IBOutlet weak var cityHiddenLabel: UILabel!

    
    internal func configure(_ topText: String, bottomText: String, nameText: String, cityText: String) {
        topLabel.text = topText
        bottomLabel.text = bottomText
        nameHiddenLabel.text = nameText
        cityHiddenLabel.text = cityText
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
