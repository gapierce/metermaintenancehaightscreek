//
//  InstalledMetersTableViewController.swift
//  MeterMaintenance
//
//  Created by Gregory Pierce on 9/19/16.
//  Copyright © 2016 Greg Pierce. All rights reserved.
//

import UIKit

class InstalledMetersTableViewController: UITableViewController {
    
    //MARK: Properties
    @IBOutlet weak var doneButton: UIBarButtonItem!

    var meters = [InstalledMeters]()
    
    var urlBaseString = "https://services.weberbasin.com/haightscreek/newdevelopmentmeter.svc/json/meters/getall/0/0/0/0/0/0"
    //var urlBaseString = "http://gisprog/newdevelopmentmeter.svc/json/meters/getall/0/0/0/0/0/0"
    var data: Data?

    override func viewDidLoad() {
        super.viewDidLoad()

            if (self.testURL(self.urlBaseString)) {
            
                self.data =  try? Data(contentsOf: URL(string: self.urlBaseString)!)
                
                let json = try! JSONSerialization.jsonObject(with: self.data!, options:.allowFragments) as! [String:AnyObject]
                
                
                if let returnedObjects = json["NewDevMeterMethodResult"] as? [[String:AnyObject]]
                {
                    for meter in returnedObjects {
                        let thisMeter = InstalledMeters()
                        thisMeter.subdivision = meter["subdivision"] as! String
                        thisMeter.address = meter["lot"] as! String
                        thisMeter.meterNumber = meter["meter"] as! String
                        thisMeter.ertNumber = meter["ert"] as! String
                        thisMeter.multiplier = meter["multiplier"] as! String
                        self.meters.append(thisMeter)
                    }
                }
                
            }
        
        self.tableView.reloadData()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    //MARK: Actions
    @IBAction func doneButtonPressed(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: Functions
    
    func testURL(_ requestURL: String) -> Bool{
        let url: URL = URL(string: requestURL)!
        let request: URLRequest = URLRequest(url: url)
        var response: URLResponse?
        
        do {
            var data = try NSURLConnection.sendSynchronousRequest(request, returning: &response) as Data?
            
            if let httpResponse = response as? HTTPURLResponse
            {
                if httpResponse.statusCode == 200
                {
                    return true
                }
                else
                {
                    return false
                }
            }
        }
        catch {}
        
        return false
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return meters.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! InstalledMetersTableViewCell

        let meter = meters[(indexPath as NSIndexPath).row]
        
        var subdivisionString: String = meter.subdivision
        subdivisionString = subdivisionString.replacingOccurrences(of: "_", with: " ")
        
        var addressString: String = meter.address
        addressString = addressString.replacingOccurrences(of: "_", with: " ")
        
        cell.configure(subdivisionString + " : " + addressString, bottomText: "Meter: " + meter.meterNumber + " ERT: " + meter.ertNumber)
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
